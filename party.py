# This file is part of Tryton. The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
"Party Rollup Organization"
import copy
from trytond.model import ModelView, ModelSQL, fields
from trytond.pyson import Equal, Eval, Not, Or, Bool
from trytond.backend import TableHandler
from trytond.transaction import Transaction
from trytond.pool import Pool


STATES = {
        'readonly': Or(Not(Bool(Eval('active'))),
                Not(Equal(Eval('party_type'), 'organization'))),
        'invisible': Not(Equal(Eval('party_type'), 'organization')),
    }
DEPENDS = ['active', 'party_type']


class Party(ModelSQL, ModelView):
    _name = 'party.party'

    parent_org = fields.Many2One('party.party', 'Parent Organization',
            select=1, domain=[
                    ('party_type', '=', 'organization'),
            ], states=STATES, depends=DEPENDS)
    child_orgs = fields.One2Many('party.party', 'parent_org',
            'Child Organizations', readonly=True, domain=[
                    ('party_type', '=', 'organization'),
            ], states=STATES, depends=DEPENDS)

    def __init__(self):
        super(Party, self).__init__()
        self.party_type = copy.copy(self.party_type)
        if 'parent_org' not in self.party_type.on_change:
            self.party_type.on_change.append('parent_org')
        if 'child_orgs' not in self.party_type.on_change:
            self.party_type.on_change.append('child_orgs')
        self._reset_columns()
        self._constraints += [
                ('_check_recursion', 'recursive_organizations'),
        ]
        self._error_messages.update({
                'recursive_organizations': 'You can not create recursive '
                                          'organizations!',
        })

    def init(self, module_name):
        # Migration from 1.6: remove MPTT from parent_org
        table = TableHandler(Transaction().cursor, self, module_name)
        for column in ['left_org', 'right_org']:
            if table.column_exist(column):
                table.index_action(column, action='remove')
                table.drop_column(column)
        super(Party, self).init(module_name)

    def _check_recursion(self, ids, parent='parent'):
        return super(Party, self).check_recursion(ids, parent='parent_org')

    def default_active(self):
        return True

    def on_change_party_type(self, values):
        res = super(Party, self).on_change_party_type(values)
        if not values.get('party_type') \
                or values.get('party_type') == 'person':
            res['parent_org'] = False
        return res

    def create(self, vals):
        if vals:
            vals = vals.copy()
        if 'party_type' in vals and vals['party_type'] == 'person':
            if 'parent_org' in vals:
                del vals['parent_org']
            if 'child_org' in vals:
                del vals['child_org']
        return super(Party, self).create(vals)

    def write(self, ids, vals):
        _ids = ids
        if type(ids) in [int, long]:
            _ids = [ids]
        party_obj = Pool().get('party.party')
        for id in _ids:
            if 'party_type' in vals and vals['party_type'] == 'person':
                # Reset the parent of children parties
                child_party_ids = party_obj.search([('parent_org.id', '=', id)])
                child_parties = party_obj.browse(child_party_ids)
                for party in child_parties:
                    party_obj.write(party.id, {'parent_org': False})
        return super(Party, self).write(ids, vals)

Party()


